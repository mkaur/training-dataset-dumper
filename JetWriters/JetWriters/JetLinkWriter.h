#ifndef JET_ASSOCIATION_WRITER_H
#define JET_ASSOCIATION_WRITER_H

#include "JetConstituentWriter.h"
#include "JetLinkWriterConfig.h"

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"
#include "xAODBase/IParticleContainer.h"

namespace H5 {
  class Group;
}

template <
  typename ConstituentType,
  typename LinkedType = xAOD::IParticleContainer,
  typename AssociationConfig = std::tuple<>>
class JetLinkWriter: public IJetLinkWriter
{
public:
  JetLinkWriter(H5::Group& output_group,
                const JetLinkWriterConfig& config,
                AssociationConfig = {});
  ~JetLinkWriter();
  JetLinkWriter(JetLinkWriter&) = delete;
  JetLinkWriter operator=(JetLinkWriter&) = delete;
  JetLinkWriter(JetLinkWriter&&);
  void write(const xAOD::Jet& jet) override;
  void flush() override;
private:
  using Links = std::vector<ElementLink<LinkedType>>;
  JetConstituentWriter<ConstituentType, AssociationConfig> m_writer;
  SG::AuxElement::ConstAccessor<Links> m_accessor;
};


#include "JetLinkWriter.icc"

#endif
